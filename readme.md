# Tiki Docker Development

This is a Docker Development environment for Tiki (development) is accessible on `tiki.local` and will run by default using php 7.4

For the full list of current options, check the comments on `.env.dist`

## Basic Configuration

Copy the `.env.dist` to `.env` and set your `TIKI_PATH` to the path where tiki code is located in your system.

Add the following line to your hosts file

```
127.0.0.1     tiki.local
```

## Starting the containers

```
docker-composer up
``` 

## Stoping the containers
```
docker-composer down
``` 

## Installing SSL certificates (*.t)
```
cd docker/https/ssl
mkcert -install
```

## Choosing the services you want to run

This environment allows you to choose a few different configurations, by default will start Apache + PHP7.4 + Mysql only, but you can choose to start a combination of:

* php56,php72,php73,php74,php80,php81,php82: Different versions of PHP
* mysql: MySQL server (internal DNS `tiki_mysql`)
* mariadb: MariaDB server (internal DNS `tiki_mariadb`)
* elasticsearch: Elastic Search (internal DNS `tiki_es`)
* email: Local email development (send and view emails locally) (internal DNS `tiki_email`)

### Update your hosts file

Below the full list of dns used, you can add that to your host files to be on the safe side

```
127.0.0.1    tiki.local 56.tiki.local 72.tiki.local 73.tiki.local 74.tiki.local 80.tiki.local 81.tiki.local 82.tiki.local webmail.tiki.local
```

### Choosing the services you want to run

> !! Ideally you should stop the containers, make your changes and start again.

copy `.env.dist` to `.env` if you didn't do yet, and make sure you update `TIKI_PATH` to point to the tiki folder

Then edit the file and set the services you want to run:

```
# What containers you want to run ?
# By default run with a minimal setup: Apache, PHP 7.4 and MySQL
# Existing Profiles: php56,php72,php73,php74,php80,php81,php82,mysql,mariadb,elasticsearch,email
COMPOSE_PROFILES=php74,mysql
```

* Example: Running PHP 7.4 (that is the default) + Mysql + Elastic Search
```
COMPOSE_PROFILES=php74,mysql,elasticsearch
```

* Example: Running PHP 7.4 (that is the default) + PHP 8.0 + Mysql + Elastic Search + email
```
COMPOSE_PROFILES=php74,php80,mysql,elasticsearch,email
```
> * You can load pages using PHP 7.4 using http://tiki.local or http://74.tiki.local
> * You can load pages using PHP 8.0 using http://80.tiki.local
> * You can check the webmail interface using http://webmail.tiki.local

## Running multiple instances (Preferred/easy option) 

## Running multiple instances (Alternative option)

For example, lets setup another instance of for Tiki 23.x - https://23x.tiki.local

Update the hosts file
```
127.0.0.1       trunk.tiki.local
127.0.0.1       23x.tiki.local
```

Mount the volumes in apache and php container, in `docker-compose.yml` file.

```
volumes:
  - ${TIKI_PATH}:/var/www/html/trunk
  - /path/to/tiki/23x:/var/www/html/23x
```

Inside the folder `docker/http/` duplicate the file `trunk.tiki.local.conf` with the name `23x.tiki.local.conf` and replace the word `trunk` with `23x`.
