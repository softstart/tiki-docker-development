#!/bin/bash

VERSIONS="56 72 73 74 80 81 82 83"

(
  cat part.header
  for V in $VERSIONS ; do
    cat part.version | tr "56" "${V}"
  done
)
