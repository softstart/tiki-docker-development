#!/bin/bash

if [ ! -d /srv/webmail/rainloop ] ; then
	(
		cd /srv/webmail
		curl -sL https://www.rainloop.net/repository/webmail/rainloop-latest.zip > /srv/webmail/rainloop-latest.zip
		unzip rainloop-latest.zip
		V=$(ls -d1 rainloop/v/* | tail -1)
		rm ${V}/app/domains/*
		cp /srv/webmail-install/domains/* ${V}/app/domains/
		cp ${V}/index.php.root index.php
	)
fi

exec php-fpm

