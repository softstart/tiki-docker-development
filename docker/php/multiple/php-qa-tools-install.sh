#!/bin/bash

#
# Composer
#

EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig)
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi

# use version 1.10.16 to keep it pin to version 1
php composer-setup.php --quiet --install-dir=/usr/local/bin --filename=composer.1 --version=1.10.16
# install version 2
php composer-setup.php --quiet --install-dir=/usr/local/bin --filename=composer
rm composer-setup.php
